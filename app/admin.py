# Tutaj rejestrujemy nasze modele.
# Po zarejestrowaniu model jest dostępny z poziomu panelu administratora django.
from django.contrib import admin
from app.models import UserProfile

admin.site.register(UserProfile)