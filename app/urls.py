from django.urls import path, include
from django.contrib.auth.views import LoginView, LogoutView

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("login/", views.login),
    path("login2/", LoginView.as_view(template_name = "app/login2.html"), name="login2"),
    path("logout2/", LogoutView.as_view(template_name = "app/logout2.html"), name="logout2"),
    path("register2/", views.register, name = "register2"),
    path("profile2/", views.view_profile, name="view_profile2"),
    path("profile2/edit/", views.edit_profile, name="edit_profile2"),
    path("change-password", views.change_password, name = "change_password")
]