# Tutaj tworzymy nasze modele
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
    """Model reprezentujący profil użytkownika.
    Profil użytkownika zawiera dodatkowe informacje dotyczące użytkownika."""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    website = models.URLField(default='')
    age = models.IntegerField(default=0)

    def create_profile(sender, **kwargs):
        """Funkcja (signal handler) uruchamiana kiedy zostanie wykryty
        sygnał post_save wysłany przez model User
        """
        if kwargs['created']:
            UserProfile.objects.create(user=kwargs['instance'])

    """Powiązanie sygnału post_save wysyłanego przez model User z funkcją
    create_profile (signal handler sygnału post_save
    """
    post_save.connect(create_profile, sender=User)

    def __str__(self):
        return f"{self.user}"