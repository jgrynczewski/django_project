# Tutaj definiujemy nasze widoki
from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

from app.forms import RegistrationForm, EditProfileForm


def home(request):
    """Widok dla strony domowej."""
    return render(request, 'app/home.html')


def login(request):
    """Widok logowania - wersja pierwsza"""
    return render(request, 'app/login.html')


def register(request):
    """Widok rejestracji użytkownika. Wyświetla formularz rejestracji."""
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("login2")
    else:
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'app/reg_form.html', args)


def view_profile(request):
    """Widok profilu użytkownika."""
    args = {'user': request.user}
    return render(request, "app/profile.html", args)


def edit_profile(request):
    """Widok edycji profilu użytkownika.
    Wyświetla formularz do edycji profilu."""
    if request.method == "POST":
        form = EditProfileForm(request.POST, instance = request.user)
        if form.is_valid():
            form.save()
        else:
            return HttpResponse("Formularz został wypełniony nieprawidłowo.")
        return redirect('view_profile2')
    else:
        form = EditProfileForm(instance = request.user)
        args = {"form": form}
        return render(request, 'app/edit_profile.html', args)


def change_password(request):
    """Widok dla zmiany hasła użytkownika.
    Wyświetla formularz zmiany hasła użytkownika."""
    if request.method == "POST":
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
        else:
            return HttpResponse("Formularz został wypełniony nieprawidłowo.")
        return redirect('view_profile2')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {"form": form}
        return render(request, 'app/change_password.html', args)